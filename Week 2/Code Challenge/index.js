const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {

  return data.filter(x => x);
}

// Should return array
function sortAscending(data) {
  // Code Here
  for (let i = 0; i < data.length; i++) {
		for (let j = 0; j < data.length; j++) {
			if (data[j] > data[j + 1]) {
				let temp = data[j];
				data[j] = data[j + 1];
				data[j + 1] = temp;
			}
		}
	}
  return data.filter(x => x);
}

// Should return array
function sortDecending(data) {
  // Code Here
  for (let i = 0; i < data.length; i++) {
		for (let j = 0; j < data.length; j++) {
			if (data[j] < data[j + 1]) {
				let temp = data[j];
				data[j] = data[j + 1];
				data[j + 1] = temp;
			}
		}
	}

  return data.filter(x => x);
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
