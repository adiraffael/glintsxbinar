const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});


function cube(side) {
  return Math.pow(side, 3);
}

function inputSide() {
  rl.question(`Side(cm): `, (side) => {
    if (!isNaN(side)) {
      console.log(`\nCube Volume: ${cube(side)} cm³`);
      rl.close();
    } else {
      console.log(`Side must be a number\n`);
      inputSide();
    }
  });
}


console.log(`Cube`);
console.log(`=========`);
inputSide();