const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});


function tube(radius, height) {
  return Math.PI * Math.pow(radius, 2) * height;
}

function inputRadius() {
  rl.question(`Radius(cm): `, (radius) => {
    if (!isNaN(radius)) {
      inputHeight(radius)
    } else {
      console.log(`Radius must be a number\n`);
      inputRadius();
    }
  });
}

function inputHeight(radius) {
  rl.question(`Height(cm): `, (height) => {
    if (!isNaN(height)) {
      console.log(`\nTube Volume: ${tube(radius, height)} cm³`);
      rl.close();
    } else {
      console.log(`Height must be a number\n`);
      inputHeight(radius);
    }
  });
}


console.log(`Tube`);
console.log(`=========`);
inputRadius();