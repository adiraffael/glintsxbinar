let covid19 = [
    {
        "name": "John",
        "status": "Positive"
    },
    {
        "name": "Mike",
        "status": "Suspect"
    },
    {
        "name": "Abdul",
        "status": "Negative"
    },
    {
        "name": "Bambang",
        "status": "Suspect"
    },
    {
        "name": "Cecep",
        "status": "Negative"
    },
    {
        "name": "Dhea",
        "status": "Suspect"
    },
    {
        "name": "Eko",
        "status": "Positive"
    },
    {
        "name": "Falah",
        "status": "Suspect"
    },
    {
        "name": "Gilang",
        "status": "Negative"
    },
]


let statusPerson = 'Positive';

switch (statusPerson) {
    case "Positive":
        const positif = covid19.filter(person => person.status === 'Positive');
        console.log(positif.map(name => name.name));
    break;
    case "Suspect":
        const suspek = covid19.filter(person => person.status === 'Suspect');
        console.log(suspek.map(name => name.name));
    break;
    case "Negative":
        const negatif = covid19.filter(person => person.status === 'Negative');
        console.log(negatif.map(name => name.name));
    break;
}