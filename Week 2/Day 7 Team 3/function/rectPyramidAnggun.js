const index = require("../index");

function rectPyramid(p, l, t) {
    const luasAlas = p * l;
    const volume = luasAlas * t;
    return volume;
}

function inputPyramid() {
    index.rl.question("Length (Cm): ", (length) => {
        index.rl.question("Width (Cm): ", (width) => {
            index.rl.question("Height (cm): ", (height) => {
                if (!isNaN(length) && !isNaN(width) && !isNaN(height)) {
                    console.log(`\nRectangle Pyramid Volume: ${rectPyramid(length, width, height)} cm³. \n`);
                    index.rl.close();
                } else {
                    console.log(`Length, Width, Height must be a number\n`);
                    inputPyramid();
                }
            });
        });
    });
}

module.exports = { inputPyramid };