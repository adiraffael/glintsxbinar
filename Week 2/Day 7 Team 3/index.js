const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const cone = require('./function/coneFalah');
const tube = require('./function/tubeAdi');
const rectPyramid = require('./function/rectPyramidAnggun');

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}

function menu() {
  console.log(`Menu`);
  console.log(`====`);
  console.log(`1. Cone`);
  console.log(`2. Tube`);
  console.log(`3. Rectangle Pyramid`);
  console.log(`4. Exit`);
  rl.question(`Choose option: `, (option) => {
    if (!isNaN(option)) {
      if (option == 1) {
        cone.inputCone();
      } else if (option == 2) {
        tube.inputRadius();
      } else if (option == 3) {
        rectPyramid.inputPyramid(); 
      } else if (option == 4) {
        rl.close();
      } else {
        console.log(`Option must be 1 to 4!\n`);
        menu();
      }
    } else {
      console.log(`Option must be number!\n`);
      menu();
    }
  });
}

menu();

module.exports.rl = rl;
module.exports.isEmptyOrSpaces = isEmptyOrSpaces;
