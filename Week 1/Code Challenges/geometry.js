function cube(side) {
    return Math.pow(side, 3);
}

function tube(radius, height) {
    return Math.PI * Math.pow(radius, 2) * height;
}

let cubeVolume1 = cube(10);
console.log("The volume of the cube1 is " + cubeVolume1 + " cm³");
let cubeVolume2 = cube(15);
console.log("The volume of the cube2 is " + cubeVolume2 + " cm³");

console.log('----------------------------------------------------');

let tubeVolume1 = tube(7, 10);
console.log("The volume of the tube1 is " + tubeVolume1 + " cm³");
let tubeVolume2 = tube(10, 13);
console.log("The volume of the tube2 is " + tubeVolume2 + " cm³");