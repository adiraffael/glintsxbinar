const express = require('express');
const app = express();

const clients = require('./routes/clients');

const port = process.env.PORT || 3000;

app.use(express.json());

app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use('/clients', clients);

app.listen(port, () => {
  console.log(`Server running smoothly on port ${port}`);
});
