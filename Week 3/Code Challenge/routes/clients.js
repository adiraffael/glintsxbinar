const express = require('express');

const {
  getAllClient,
  getDetailClient,
  addClient,
  updateClient,
  deleteClient,
} = require('../controllers/clients');

const router = express.Router();

router.get('/', getAllClient);

router.get('/:id', getDetailClient);

router.post('/', addClient);

router.put('/:id', updateClient);

router.delete('/:id', deleteClient);

module.exports = router;
