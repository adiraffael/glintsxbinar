let data = require('../models/data.json');

class Client {
  getAllClient(req, res, next) {
    try {
      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ['Internal Server Error'],
      });
    }
  }

  getDetailClient(req, res, next) {
    try {
      const detailData = data.filter((item) => item.id == req.params.id);

      if (detailData.length === 0) {
        return res.status(404).json({ errors: ['Client not found'] });
      }
      res.status(200).json({ data: detailData });
    } catch (error) {
      res.status(500).json({
        errors: ['Internal Server Error'],
      });
    }
  }

  addClient(req, res, next) {
    try {
      let lastId = data[data.length - 1].id;

      data.push({
        id: lastId + 1,
        name: req.body.name,
        gender: req.body.gender,
        address: req.body.address,
        city: req.body.city,
        job: req.body.job
      });

      res.status(201).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ['Internal Server Error'],
      });
    }
  }

  updateClient(req, res, next) {
    try {
      let findClient = data.some((item) => item.id == req.params.id);

      if (!findClient) {
        return res.status(404).json({ errors: ['Client not found'] });
      }

      data = data.map((item) =>
        item.id == req.params.id
          ? {
            id: req.params.id,
            name: req.body.name,
            gender: req.body.gender,
            address: req.body.address,
            city: req.body.city,
            job: req.body.job
            }
          : item
      );

      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ['Internal Server Error'],
      });
    }
  }

  deleteClient(req, res, next) {
    try {
      let findClient = data.some((item) => item.id == req.params.id);

      if (!findClient) {
        return res.status(404).json({ errors: ['Client not found'] });
      }

      data = data.filter((item) => item.id != req.params.id);

      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ['Internal Server Error'],
      });
    }
  }
}

module.exports = new Client();
