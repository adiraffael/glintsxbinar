import fetch from 'node-fetch';

let urlConfirmed = 'https://covid19.mathdro.id/api/confirmed';
let urlDeaths = 'https://covid19.mathdro.id/api/deaths';
let urlDaily = 'https://covid19.mathdro.id/api/daily';

let data = {};

fetch(urlConfirmed)
  .then(confirmed => confirmed.json())
  .then((response) => {
    data = { confirmed: response.filter((item) => item.countryRegion === 'Indonesia') };

    return fetch(urlDeaths);
  })
  .then(deaths => deaths.json())
  .then((response) => {
    data = { ...data, deaths: response.filter((item) => item.countryRegion === 'Indonesia') };

    return fetch(urlDaily);
  })
  .then(daily => daily.json())
  .then((response) => {
    data = { ...data, daily: response.filter((item) => item.reportDate === '2020-10-20') };

    console.log(data);
  })
  .catch((err) => {
    console.log(err.message);
  });
