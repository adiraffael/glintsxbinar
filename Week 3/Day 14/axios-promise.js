import axios from 'axios';

let urlConfirmed = 'https://covid19.mathdro.id/api/confirmed';
let urlDeaths = 'https://covid19.mathdro.id/api/deaths';
let urlDaily = 'https://covid19.mathdro.id/api/daily';
let data = {};

axios
  .get(urlConfirmed)
  .then((response) => {
    data = { confirmed: response.data.filter((item) => item.countryRegion === 'Indonesia') };

    return axios.get(urlDeaths);
  })
  .then((response) => {
    data = { ...data, deaths: response.data.filter((item) => item.countryRegion === 'Indonesia') };

    return axios.get(urlDaily);
  })
  .then((response) => {
    data = { ...data, daily: response.data.filter((item) => item.reportDate === '2020-10-20') };

    console.log(data);
  })
  .catch((err) => {
    console.log(err.message);
  });
