import axios from 'axios';

const fetchApi = async () => {
  let urlConfirmed = 'https://covid19.mathdro.id/api/confirmed';
  let urlDeaths = 'https://covid19.mathdro.id/api/deaths';
  let urlDaily = 'https://covid19.mathdro.id/api/daily';
  
  let data = {};

  try {
    const response = await Promise.all([
      axios.get(urlConfirmed),
      axios.get(urlDeaths),
      axios.get(urlDaily),
      
    ]);

    data = {
      confirmed: response[0].data.filter((item) => item.countryRegion === 'Indonesia'),
      deaths: response[1].data.filter((item) => item.countryRegion === 'Indonesia'),
      daily: response[2].data.filter((item) => item.reportDate === '2020-10-20'),
      
    };

    console.log(data);
  } catch (error) {
    console.error(error.message);
  }
};

fetchApi();
