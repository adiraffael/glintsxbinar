import fetch from 'node-fetch';

const fetchApi = async () => {
    const urlConfirmed = await fetch('https://covid19.mathdro.id/api/confirmed');
    const urlDeaths = await fetch('https://covid19.mathdro.id/api/deaths');
    const urlDaily = await fetch('https://covid19.mathdro.id/api/daily');

    let data = {}

try {
    const response = await Promise.all([
        urlConfirmed.json(),
        urlDeaths.json(),
        urlDaily.json(),
    ])

    data = {
        confirmed: response[0].filter((item) => item.countryRegion === 'Indonesia'),
        deaths: response[1].filter((item) => item.countryRegion === 'Indonesia'),
        daily: response[2].filter((item) => item.reportDate === '2020-10-20'),
    };

    console.log(data);

} catch (error) {
    console.error(error.message);
}
};

fetchApi();